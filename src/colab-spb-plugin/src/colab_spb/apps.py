
from django.apps import AppConfig


class SpbAppConfig(AppConfig):
    name = 'colab_spb'
    verbose_name = 'SPB'
    namespace = 'spb'
